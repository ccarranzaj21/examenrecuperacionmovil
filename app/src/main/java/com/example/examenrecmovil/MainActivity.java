package com.example.examenrecmovil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView lblNombre;
    private EditText txtNom;
    private Button btnSalir;
    private Button btnEntrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblNombre = findViewById(R.id.lblNombre);
        txtNom = findViewById(R.id.txtNom);
        btnEntrar = findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(v -> {


            if (txtNom.getText().toString().isEmpty()) {
                Toast.makeText(MainActivity.this, "Por favor ingrese el nombre de usuario", Toast.LENGTH_SHORT).show();
            } else {

                Intent intent = new Intent(MainActivity.this, cotizacionActivity.class);
                intent.putExtra("nombre", txtNom.getText().toString());
                startActivity(intent);
            }

        });


    }
    }

